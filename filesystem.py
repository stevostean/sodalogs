from pprint import pprint

import os

root = os.path.abspath(os.path.dirname(__file__))
repo_dir = os.path.join(root, "analytics")


def logSlice(log_list):
    log_counter = month = year = 0

    for line in log_list:
        if line['Date'].year != year:
            year = line['Date'].year
        first_dir = createNuDir(year)

        if line['Date'].month != month:
            month = line['Date'].month
            file_name = os.path.join(first_dir, nuFileName(month))
            # print(">>> Create {}.".format(file_name))
            pprint(dict(created_filename=file_name))

        f = open(file_name, 'a')
        inline = ",".join(["=".join([k, str(v)]) for k, v in line.items()])
        if inline != "":
            f.write("{}\n".format(inline))
        f.close()
        log_counter += 1

    # print(">>> {} records made in {}.".format(fileDepth(file_name), file_name))
    # print(">>> Total: {} logs processed.".format(log_counter))
    status = dict(records=fileDepth(file_name),
                  total=log_counter)
    pprint(status)


def createNuDir(dir_name):
    dir_name = dir_name if isinstance(dir_name, str) else str(dir_name)

    if os.path.exists(os.path.join(repo_dir, dir_name)) == False:
        nu_dir = os.path.join(repo_dir, dir_name)
        os.makedirs(nu_dir)
        # print(">>> Create new directory: {}".format(nu_dir))
        pprint(dict(new_directory=nu_dir))
        return nu_dir

    return os.path.join(repo_dir, dir_name)


def fileDepth(f):
    """
    return max(enumerate(open(f)))[0]
    """

    with open(f) as fi:
        return len(fi.readlines())


def nuFileName(s):
    s = s if isinstance(s, int) else int(s)
    return "{num:02d}.log".format(num=s)
