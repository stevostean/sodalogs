import inspect

"""
# Inspect is a great library to check
# everything you need about functions and classes

FROM: http://stackoverflow.com/questions/6810999/how-to-determine-file-function-and-line-number#answer-6811020

"""

def Message():

    callerFrameRecord = inspect.stack()[1]
    frame = callerFrameRecord[0]

    info = inspect.getframeinfo(frame)

    return "Error from {}, in {} function, on line {}".format(info.filename, info.function, str(info.lineno))


if __name__ == '__main__':
    print(Message())
