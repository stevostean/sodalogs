#!/bin/bash

echo "Installation des packages Python"
sudo pip3 install -r requirements.txt

echo "Mise à jour de la base Redis avec les logs distants"
python3 persistance.py

echo "Symlink pour multitask.py dans /usr/local/bin/sodabar_multitask"
sudo ln -s /srv/py/sodabar/multitask.py /usr/local/bin/sodabar_multitask
sudo chmod u+x /usr/local/bin/sodabar_multitask

echo "Création des répertoires uWsgi (log & run)"
sudo mkdir -p /run/uwsgi & sudo mkdir -p /var/log/uwsgi
sudo chown -R www-data:www-data /run/uwsgi # DEBIAN/UBUNTU
sudo chown -R www-data:www-data /run/uwsgi # DEBIAN/UBUNTU
#sudo chown -R root:http /var/log/uwsgi
#sudo chown -R root:http /var/log/uwsgi #CENTOS/FEDORA/ARCHLINUX

echo "Préparation pour uWsgi..."
sudo mkdir -p /etc/uwsgi/vassals 
sudo ln -s /srv/py/sodabar/services/sodabar_api_server.ini /etc/uwsgi/vassals/sodabar_api_server.ini

echo "Symlink du service uWsgi vers Supervisor"
#sudo ln -s /srv/py/sodabar/services/uwsgi.service /etc/systemd/system/uwsgi.service
sudo ln -s /srv/py/sodabar/supervisor/conf.d/uwsgi_sodabar.conf /etc/supervisor/conf.d/uwsgi_sodabar.conf

echo "Démarrage et status de Supervisor avec uWsgi..."
sudo supervisorctl update
sudo supervisorctl start uwsgi
sudo supervisorctl status uwsgi

sudo ln -s /srv/py/sodabar/services/sodabar_logs.service /etc/systemd/system/sodabar_logs.service
sudo ln -s /srv/py/sodabar/services/sodabar_logs.timer /etc/systemd/system/sodabar_logs.timer

sudo systemctl start sodabar_logs.timer
sudo systemctl enable sodabar_logs.timer

sudo ln -s /srv/py/sodabar/nginx/sodabar_api /etc/nginx/sites-enabled/sodabar_api
sudo nginx -t
sudo service nginx reload

