from calendar import monthrange

import pandas as pd

from persistance import *


class LogData:
    """
    Take data from Redis Server then handle it with Pandas!
    """

    def __init__(self):
        p = fromRedis()

        self._infos = p[1]

        self._data = pd.DataFrame(p[0])
        self._data.set_index('date', inplace=True, drop=False)
        self._data.index = pd.to_datetime(self._data.index, unit='s')
        self._data.drop('latitude', axis=1, inplace=True)
        self._data.drop('longitude', axis=1, inplace=True)

    def __getattr__(self, item):
        return self._data[item]

    @property
    def index(self):
        # return self._data.date
        return self._data.index

    @property
    def redis(self):
        return self._infos

    def get_by_date(self, month=None, year=None):
        """
        Get a pandas object from a month's range in a year
        Args:
            month: <int> the month number (1-12)
            year: <int> the year number (4 digits)

            if None of them:
                it returns an array of each year's result

        Returns: <pandas> object to play with...

        """
        if month == None and year == None:
            return self._classified_data_by_years()

        _, end_m = monthrange(year, month)

        # if start_m < 1:
        start_m = 1

        startpoint = "{}{num:02d}{n:02d}".format(year, num=month, n=start_m)
        endpoint = "{}{num:02d}{n:02d}".format(year, num=month, n=end_m)

        return self._loc_data(startpoint, endpoint)

    def _classified_data_by_years(self):
        '''
        PRIVATE METHOD: get classified data without any details
        Returns: <list> each index is a year's resulting data

        '''
        year_min = self.index.min().year
        year_max = self.index.max().year
        result_by_year = ()
        while year_min <= year_max:
            startpoint = "{}0101".format(year_min)
            endpoint = "{}1231".format(year_min)
            result_by_year += (year_min, (self._loc_data(startpoint, endpoint))),
            year_min += 1
        return result_by_year

    def _loc_data(self, s, e):
        '''
        PRIVATE METHOD: simple way of loc the DataFrame on 2 periods,
        according to the self dataset
        Args:
            s: starting point (datetime def)
            e: ending point (datetime def)

        Returns: <dataframe> reclassified dataframe on a s
        tarting date & ending date (2016-01-01 & 2016-31-01)

        '''
        return self._data.loc[s:e, list(self._data.columns)]

    def collection(self, type='D'):
        '''
        GET all  raw counts from the main DataFrame
        Args:
            type: <string> D: days | M: Months eg: '3M' = trimestre

        Returns: <dataframe> with datetime index, counts & cumuls

        '''
        element = pd.DataFrame(self.index.value_counts(),
                               index=self.index)
        element.rename(columns={'date': 'date', 'date': 'count'},
                       inplace=True)

        count_elements = element.resample(type, base=1).sum()
        count_elements.rename(columns={'count': 'counts'},
                              inplace=True)

        cumsum_element = count_elements.cumsum()
        cumsum_element.rename(columns={'counts': 'cumuls'},
                              inplace=True)

        return pd.concat([count_elements, cumsum_element], axis=1)


if __name__ == '__main__':
    ld = LogData()
    print(ld.get('city'))
    print(ld.get_by_date(month=3, year=2016))
