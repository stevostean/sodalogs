import datetime
import inspect
from flask import Flask, jsonify, request, url_for, Response 
from werkzeug.exceptions import default_exceptions, HTTPException

import system_info
from kungfupanda import *

app = Flask(__name__)
app.config['PROPAGATE_EXCEPTIONS'] = True

"""
Error Handlers in JSON format

"""

__all__ = ['make_json_app']

def make_json_app(import_name, **kwargs):
    """
    Creates a JSON-oriented Flask app.

    All error responses that you don't specifically
    manage yourself will have application/json content
    type, and will contain JSON like this (just an example):

    { "message": "405: Method Not Allowed" }
    """
    def make_json_error(ex):
        response = jsonify(message=str(ex))
        """
        response.status_code = (ex.code
                                if isinstance(ex, HTTPException)
                                else 500)
        """
        return response

    app = Flask(import_name, **kwargs)

    for code in default_exceptions.iterkeys():
        app.error_handler_spec[None][code] = make_json_error

    return app

@app.errorhandler(404)
def page_not_found():
    return jsonify(dict(status=404, msg="Bad Request: no route defined"))


@app.before_request
def report():
    app.api_report = LogData()


def stats(func, *args, **kwargs):
    r = app.api_report

    try:
        gen_stats = getattr(r, func)(*args, **kwargs)
        total = r.total
        vby = r.visits_by_year()

        out = dict(data=gen_stats,
                   total=total,
                   visits_by_year=vby)
    except AttributeError:
        out = dict(error=404,
                   msg='No attribute')

    return out


def json_output(content, type='application/json', status=200, *args, **kwargs):
    return Response(response=content.to_json(),
                    status=status,
                    mimetype=type)

"""
AFTER REQUEST: send CORS headers to allow web apps to get data for Ajax.
"""
@app.after_request
def cors_headers(response):
#    response.headers.add('Content-Type', 'application/json')
    response.headers.add('Access-Control-Allow-Origin', '*')
    response.headers.add('Access-Control-Allow-Headers', 'Content-Type,Authorization')
    response.headers.add('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE')
    return response

"""
AFTER REQUEST: Cache-control for client / browser
"""
@app.after_request
def cache_control(response):
    data = app.api_report
    Redis = data.redis
    isCached = Redis['cached']
    lastUpdate = Redis['last_redis_update']['timestamp']
    nextUpdate = datetime.datetime.utcfromtimestamp(int(lastUpdate)) + datetime.timedelta(minutes=80)

    if isCached:
        response.headers.add("Cache-Control", "public")
        response.headers.add("Expires", nextUpdate.strftime("%a, %d %b %Y %H:%M:%S GMT"))
    else:
        response.headers.add("Cache-Control", "no-store")

    response.headers.add("X-Powered-By", "StudioMZK/1.0")
    return response

@app.route("/.well_known/acme-challenge/<awtf>")
def ssl_check(awtf):
    return "<html><head><title>index</title></head><body><h1>whatever</h1></body></html>"

@app.route('/')
def index():
    start = time()

    big = app.api_report
    redis_infos = big.redis
    r = big.collection(type='D')
    more_stats = r.describe()
    # general = big.date.describe().to_dict()

    return jsonify(results=r.to_dict('split'),
                   stats=more_stats['counts'].to_dict(),
                   redis=redis_infos,
                   # total=general['count'],
                   details=url_for('details', _external=True),
                   exe_time=round(time() - start, 4)), 200


@app.route('/details')
def details():
    start = time()
    info = app.api_report
    general = info.get_by_date()
    # print(general)
    it = {}
    for el in general:
        lat = el[1].latency.describe()
        # Each element from Pandas DataFrame Object have to be converted to <float>
        # maybe because the first element returns <float> values
        # or dtypes of <int64> are NOT suitable to Flask's JSON format ?
        devices = el[1].device.value_counts().astype('float')
        cities = el[1].city.value_counts().head(3).astype('float')
        countries = el[1].country.value_counts().head(3).astype('float')

        view_by_month = ["{}".format(url_for('detail',
                                             year=el[0],
                                             month=it,
                                             _external=True))
                         for it in list(set(el[1].index.month))]

        it[el[0]] = dict(latency=lat.to_dict(),
                         devices=devices.to_dict(),
                         cities=cities.to_dict(),
                         countries=countries.to_dict(),
                         view_by_month=view_by_month)

    endresult = dict(data=it,
                     back=url_for('index', _external=True),
                     exe_time=time() - start)

    return jsonify(endresult)


"""
FROM: http://stackoverflow.com/questions/14032066/flask-optional-url-parameters
# url = /2015 or /2016/7

"""


@app.route('/<int:year>', defaults={'month': None})
@app.route('/<int:year>/<int:month>')
def detail(year, month=None):
    start = time()
    r = app.api_report
    result = {}

    if month is None:
        target = dict(r.get_by_date())
        err = 200
        df = target[year]
        df_latencies = df.latency.describe().astype('float').to_dict()
        df_devices = df.device.value_counts().astype('float').to_dict()
        df_cities = df.city.value_counts().head().astype('float').to_dict()
        df_countries = df.country.value_counts().head().astype('float').to_dict()
        df_count = df.groupby(df.index.date).count().drop(['ip',
                                                           'city',
                                                           'country',
                                                           'device',
                                                           'date'],
                                                          axis=1).rename(columns={'latency': 'visits'})
        count_desc = df_count.describe().astype('float')
        # print(df_count, count_desc)
        del df['date']
        del df['ip']
        # print(df)
        # df = df.astype('float').to_dict()

        ct_data = dict(stats=count_desc.to_dict(),
                       visits=df_count.astype('int').to_dict('split')
                       )

        ct_data.update(dict(latencies=df_latencies,
                            devices=df_devices,
                            cities=df_cities,
                            countries=df_countries))

        try:
            result.update({str(year): ct_data})
        except:
            err = 404
            result.update({'err': 404, 'msg': "No content."})

        result.update(dict(exe_time=time() - start))
        return jsonify(result), err

    try:
        df = r.get_by_date(year=year, month=month)
        counts = pd.DataFrame(df.index.value_counts(),
                              index=df.index)
        counts.rename(columns={'date': 'date', 'date': 'visits'},
                      inplace=True)
        counts_result = counts.resample('D').sum()
        counts_stats = counts_result.describe()

        counts_dict = {str(month): {'visits': counts_result.astype('int').to_dict('split')},
                       'stats': counts_stats.astype('float').to_dict()}
        devices = df.device.value_counts().astype('float').to_dict()
        latencies = df.latency.describe().astype('float').to_dict()
        cities = df.city.value_counts().astype('float').head().to_dict()
        countries = df.country.value_counts().astype('float').head().to_dict()

        content = {str(year): counts_dict}
        content.update(dict(latencies=latencies,
                            devices=devices,
                            cities=cities,
                            countries=countries))

        err = 200
    except:
        content = dict(error=404, msg="Content not found.")
        err = 404

    content.update(dict(exe_time=time() - start))
    return jsonify(content), err


@app.route('/cities')
@app.route('/cities/<int:nbr>')
def cities(nbr=3):
    start = time()
    func_name = inspect.stack()[0].function
    out = stats(func_name, highests=nbr)

    out.update(dict(exe_time=time() - start))

    return jsonify(out), 200


@app.route('/countries')
@app.route('/countries/<int:nbr>')
def countries(nbr=3):
    start = time()
    func_name = inspect.stack()[0].function
    out = stats(func_name, highests=nbr)

    out.update(dict(exe_time=time() - start))

    return jsonify(out), 200


@app.route('/latency')
def latency():
    start = time()
    r = app.api_report

    out = dict(exe_time=time() - start)

    return jsonify(r.latency(), out), 200


@app.route('/devices')
def devices():
    start = time()
    func_name = inspect.stack()[0].function
    out = stats(func_name)

    extra = dict(exe_time=time() - start)

    return jsonify(out, extra), 200


@app.route('/system')
@app.route('/system/<func>')
def system(func='stats'):
    print(request.endpoint, request.url, request.url_root)
    current_url = request.url_root + request.endpoint
    out = {}

    if not hasattr(system_info, func):
        lOa = ["clients",
               "cpu",
               "keyspace",
               "mem",
               "other_stats",
               "server",
               "stats"]

        loa_endpoints = ['{}/{}'.format(current_url, it) for it in lOa]

        out['failure'] = {'error': 401,
                          'msg': 'no attribute with this API.',
                          'available_commands': loa_endpoints}
    else:
        syst = getattr(system_info, func)

        out[func] = syst()

    return jsonify(out), 200

@app.route('/error')
def test_error():
    raise Exception()

"""
if __name__ == '__main__':
    app.run(debug=True)
"""
