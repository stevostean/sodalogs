#! /usr/Local/bin/python3.5

import argparse
from time import time
from pprint import pprint

from generate import init
from report import main_stats, logs_count, yearly_report, monthly_report

# from webserver import run_server
from async_webserver import web, app

def reports():
    start = time()
    content = dict()

    content.update(dict(visits=logs_count()))
    content.update(dict(population=main_stats()))
    content.update(yearly_report())
    content.update(dict(elapsed_execution_time=time()-start))

    """
    return [logs_count(True),
            main_stats(),
            yearly_report(),
            dict(elapsed_execution_time=time()-start)]
    """
    return content


if __name__ == '__main__':

    host = '127.0.0.1'
    port = 8081
    timeout = None

    parser = argparse.ArgumentParser(description="Soda-Bar CLI generator & report stats.")
    parser.add_argument('-i',
                        '--init',
                        action="store_true",
                        default=False,
                        dest='init_switch',
                        help="Get data from distant log file & process in multiple directories & files locally.")
    parser.add_argument('-R',
                        '--report',
                        default=False,
                        action="store_true",
                        dest='report_switch',
                        help="Retrieve all data from local logs, by year and months.")

    parser.add_argument('-p',
                        '--period',
                        action='store_true',
                        default=False,
                        help="Retrieve all detailed data from a month and a year, in the logs.")

    parser.add_argument('-M',
                        '--month',
                        type=int,
                        action='store',
                        default=None,
                        help="Defines the month of the monthly report (as number).")

    parser.add_argument('-Y',
                        '--year',
                        type=int,
                        action='store',
                        default=2016,
                        help="Defines the year of the monthly report (as number).")

    parser.add_argument('-s',
                        '--serve',
                        default=False,
                        action='store_true',
                        dest='server_switch',
                        help="Simulate a local server")

    parser.add_argument('-H',
                        '--host',
                        action='store',
                        default=host,
                        help="Defines the address of webserver. Default:".format(host))

    parser.add_argument('-P',
                        '--port',
                        action='store',
                        default=port,
                        type=int,
                        help="Defines the port of webserver. Default:".format(port))

    parser.add_argument('-to',
                        '--timeout',
                        action='store',
                        default=timeout,
                        type=int,
                        help="Defines the shutdown timeout of webserver (in sec). Default:".format(timeout))

    cli = parser.parse_args()

    if cli.host:
        host = cli.host
    if cli.port:
        port = cli.port
    if cli.timeout:
        timeout = cli.timeout

    if cli.init_switch:
        init()

    elif cli.report_switch:
        start = time()
        content = dict()

        content.update(dict(visits=logs_count()))
        content.update(dict(population=main_stats()))
        content.update(yearly_report())
        content.update(dict(elapsed_execution_time=time() - start))

        print(content)

    elif cli.period:
        year = 2016
        month = None
        if cli.month:
            month = cli.month
        if cli.year:
            year = cli.year

        pprint(monthly_report(year, month=month))

    elif cli.server_switch:
        web.run_app(app,
                    host=host,
                    shutdown_timeout=timeout,
                    port=port)

    else:
        print("No command passed?")
