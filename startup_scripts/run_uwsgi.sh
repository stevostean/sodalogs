#!/bin/bash

if [ ! -d /run/uwsgi ]
    then
    mkdir /run/uwsgi
    chown -r www-data:www-data /run/uwsgi
fi

