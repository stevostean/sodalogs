#!/usr/bin/python3
from persistance import tz, toRedis, fromRedis, kompakt

from datetime import datetime, timedelta
import logging
from logging.handlers import RotatingFileHandler
import os
import threading
import time

count = 1
old_tz = 0

root_path = '/srv/py/sodabar'
log_path = os.path.join(root_path, 'task_logs')
if not os.path.exists(log_path):
    os.makedirs(log_path, exist_ok=True)

logger = logging.getLogger()
fh = RotatingFileHandler(os.path.join(log_path, 'tasks.log'), 'a', 675000, 100)
# lf = logging.Formatter('%(asctime)s %(name)s %(levelname)-8s %(message)s')
lf = logging.Formatter('%(asctime)s ~> %(message)s')
fh.setFormatter(lf)
logger.setLevel(logging.WARNING)
logger.addHandler(fh)

class RedisTask:
    """
    Counting Instances of this class was possible gracefully with:
    FROM: http://www.pythonabc.com/best-way-count-instances-python-class/

    Many examples are available at this last link (staticclass, classmethod, etc.)
    """

    Number_of_Instances = 0

    def __init__(self, t, function):
        self._t = float(t * 60) # convert to minutes
        self._function = function
        #self._thread = threading.Timer(self._t, self._function_handler)
        self._function_handler()

    def _function_handler(self):
        now = datetime.now(tz=tz)
        scheduled_update = now + timedelta(seconds=int(self._t))

        if self.Number_of_Instances < 1:
            delay = 6.0
            logger.warning("=== First launching of the Multitask script in the next 6 seconds ===")
        else:
            delay = self._t
            #logger.warning("NEXT UPDATE: it must be triggered at {}".format(scheduled_update.strftime("%Y-%m-%d %H:%M:%S")))

        self._function()

        self._thread = threading.Timer(delay, self._function_handler)
        self._thread.start()
        logger.warning("NEXT UPDATE: it must be triggered at {}".format(scheduled_update.strftime("%Y-%m-%d %H:%M:%S")))
        self.incrementInstance()

    def start(self):
        self._thread.start()

    def cancel(self):
        self._thread.cancel()

    def getInstances(cls):
        return cls.Number_of_Instances

    def incrementInstance(cls):
        cls.Number_of_Instances += 1

def redisUpdate():
    now = datetime.now(tz=tz)

    ru = toRedis(kompakt(-1))
    ru_system = ru[1]

    if not ru_system['error']:
        logger.warning("NOW: Redis DB was UPDATED  at {}".format(now.strftime("%Y-%m-%d %H:%M:%S")))
        logger.warning("STATS: {} logs processed during this task.".format(ru_system['log_size']))
    else:
        try:
            msg = ru_system['msg']
        except KeyError:
            msg = "???"
        logger.warning("!!! ALARM !!! {}".format(msg))

def main():
    rt = RedisTask(20, redisUpdate)
    #rt.start()


if __name__ == "__main__":
    main()
