import random
import redis
import threading
import time


class Listener(threading.Thread):
'''
FROM: https://gist.github.com/jobliz/2596594
'''
    def __init__(self, r, channels):
        threading.Thread.__init__(self)
        self.redis = r
        self.pubsub = self.redis.pubsub()
        self.pubsub.subscribe(channels)

    def work(self, item):
        print(item['channel'], ":", item['data'])

    def run(self):
        for item in self.pubsub.listen():
            if item['data'] == "KILL":
                self.pubsub.unsubscribe()
                print(self, "unsubscribed and finished")
                break
            else:
                self.work(item)

if __name__ == "__main__":
    # changing PUB/SUB SERVER on db 1 (db 0 is for the main log database)
    r = redis.Redis(host='127.0.0.1', port=6379, db = 1)
    client = Listener(r, ['test'])
    client.start()

    messages = ['This will reach the listener',
                'The listener is always right',
                'Sometimes, I love to take advice from listener',
                'The listener cannot write something here',
                'Maybe one day the listener could not be just a listener',
                'Checking if the listener still listen here',
                'Redis is a good tool',
                'Could you listen to this channel?',
                'Tralala itou']
    random.shuffle(messages)
    i = 0

    while i < len(messages):
        r.publish('test', messages[i])
        i += 1
        time.sleep(2)

    r.publish('fail', 'this will not')

    r.publish('test', 'KILL')
