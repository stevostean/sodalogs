GET LOGS FROM A SHARED WEB SERVER FOR AN API
============================================

### Access data to webserver as JSON API: <fqdn>:8088

## (Webserver running apache + php 5.6)

There's a script in homepage to write some log infos according to the user/client access.

Everything here was written in strict **Python 3 (version 3.5.1)** and can run on dev device with *virtualenv*.
It's recommanded to install all Python's Packages on a Production Server as global (not in virtualenv).


### FOR THREADED UPDATE of Redis Object

All steps to run the API on a NGINX web server:

1- You NEED to run this script for the very first time:

```bash
python[3] persistance.py
```

it should output the compressed data array for Redis. This means it populates data into Redis server (key: sodabar)

2- Execute the Redis update Task in background:

```bash
chmod +x multitask.py
ln -s /srv/py/sodabar/multitask.py /usr/local/bin/sodabar_multitask
```

To check the task in background:

```bash
# check processes:
ps aux | grep sodabar_multitask
```

FROM: http://askubuntu.com/questions/396654/how-to-run-the-python-program-in-the-background-in-ubuntu-machine/#answer-396655

Even, check out the logs about this task (it's setup as a log rotate file):
```bash
tail -F /srv/py/sodabar/task_logs/tasks.log
```


3- Create directories for **uWSGI**:

web server privileges:

- **Archlinux:** http:http
- **Debian/Ubuntu:** www-data:www-data

```bash
mkdir -p /run/uwsgi & mkdir -p /var/log/uwsgi
chown -R [webserver privileges] /run/uwsgi
chown -R [webserver privileges] /var/log/uwsgi
```

4- Systemd & uWSGI processes:
It's now all inclusive in 'services' directory.

FOR UWSGI:
- create directory:
```bash
mkdir -p /etc/uwsgi/vassals
```
- symlink the uWSGI script:
```bash
ln -s /srv/py/sodabar/sodabar_api_server.ini /etc/uwsgi/vassals/sodabar_api_server.ini
```
- Symlink uwsgi.service script to systemd directory:

```bash
ln -s /srv/py/sodabar/services/uwsgi.service /etc/systemd/system/uwsgi.service
```

- **BEWARE:** sometimes, on **Debian Jessie** distributions for example, you MUST do this before enabling any services (or you'll always get an error):

```bash
apt-get install libpam-systemd
```

then, you CAN enable any services on **systemd** on Debian, like this:

```bash
systemctl start uwsgi.service
systemctl status uwsgi.servive
(then, you'll check everything is wroking fine)
>>> systemctl enable uwsgi.service
(you'll get a prompt about the symlink it just creates)
```



- **WARNING:** sometimes, on few OS, you have to dynamically create **/run/uwsgo** at boot (else it won't exists). So, you have to edit **/etc/rc.local** like this:

```bash
#!/bin/sh -e
# 
# rc.local
#
# This script is executed at the end of each multiuser runlevel.
# Make sure that the script will "exit 0" on success or any other
# value on error.
#
# In order to enable or disable this script just change the execution
# bits.
#
# By default this script does nothing.

if [ ! -d /run/uwsgi ]; then
  mkdir run/uwsgi/
  chown www-data:www-data /run/uwsgi/
fi
exit 0
```
(FROM: http://unix.stackexchange.com/questions/2109/create-directory-in-var-run-at-startup)

- start, test & enable systemd script:

```bash
systemd start uwsgi.service
systemd status uwsgi.service
# if ok:
systemd enable uwsgi.service
```

FOR BG DATA UPDATES TO REDIS:

- Symlink both service & timer to systemd:

```bash
ln -s /srv/py/sodabar/services/sodabar_logs.service /etc/systemd/system/sodabar_logs.service
ln -s /srv/py/sodabar/services/sodabar_logs.timer /etc/systemd/system/sodabar_logs.timer
```

- start, test & enable scheduled script:

```bash
systemctl start sodabar_logs.timer
systemctl status sodabar_logs.timer
journalctl sodabar_logs.service
systemctl enable sodabar_logs.timer
```


5- Nginx config & running:
- Symlink script to Nginx Sites Configuration directory & symlinked it:

```bash
ln -s /srv/py/sodabar/sodabar_api /etc/nginx/sites-available/sodabar_api
ln -s /etc/nginx/sites-available/sodabar_api /etc/nginx/sites-enabled/sodabar_api
nginx -t
service nginx reload
```

6- Last checks:

```bash
http :8088
# or:
curl <ip>:8088
```

**BEWARE**: the *sodabar_api_server.ini* script is written here as example. Don't follow strictly this logic according to Unix system and web server configuration!

### SUPERVISOR IS WORKING:

**Uwsgi** is now running with its config file:
> SEE: /etc/suspervisor/conf.d/uwsgi.conf

few commands to learn:
    - sudo supervisorctl reread (check if there's config files and list them)
    - sudo supervisorctl reload (to reload entire conf.d files inside)
    - sudo supervisorctl status (Check status of each process, uptime, etc.)
    - and FINALLY, if you're **re-write your http application (e.g.
      pandapi.py)**, you'll need to restart **uWsgi** like this: sudo
      supervisorctl restart uwsgi

- WIP: find a simple way to execute every 20 minutes our sodabar_multitask CLI
  (done for server part / not in javascript file)

- **28/03/2017**: 50.000 visites, 590 jours, 64.606.000 visites cumulées.

Stupidity!
