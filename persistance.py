import datetime
from dateutil import parser
import json

from error import Message
import filesystem
import msgpack
import os
import pytz
from random import random
import redis
import requests
import sys
from time import sleep, time, mktime

tz = pytz.timezone('Europe/Paris')
local_repo = os.path.join(filesystem.root, "analytics")
# r = redis.StrictRedis(charset='utf-8', decode_responses=True)

'''
According to the link below, ConnectionPool can automatically destroyed object
and close connection to Redis (maybe managing better memory?)
FROM: http://stackoverflow.com/questions/12967107/managing-connection-to-redis-from-python#answer-12973514
'''
POOL = redis.ConnectionPool(host='127.0.0.1', port=6379, db=0)
r = redis.StrictRedis(charset='utf-8', decode_responses=False, connection_pool=POOL)

s3_link = requests.get('http://oueuou.eu:3663')
URL = json.loads(s3_link.content)['url']


def kompakt(depth=1, key=None):
    """
    Get values from external log file (from: soda-bar.fr)
    :param depth: <int> -1 = ALL | None: last one | last <int> ones
    :param key: if <str> for key -> get only some values from key (Has to be Capitalize)
    :return: <list: dict> / re-format: logsSerializer
    """
    r = redis.StrictRedis(charset='utf-8', decode_responses=False)
    url = "http://soda-bar.fr/hide/loading_page.log"
    start = time()
    try:
        logfile = requests.get(URL)
        logfiledata = logfile.headers
    except requests.exceptions.RequestException as e:
        error_msg = "{}, requests.exceptions.RequestException >> {}".format(Message(), repr(e))
        return dict(error=True, msg=error_msg, exec_time=time() - start)
        sys.exit(1)
    except requests.exceptions.HTTPError as h:
        error_msg = "{}, request.exceptions.HTTPError >> {}".format(Message(), repr(h))
        return dict(error=True, msg=error_msg, exec_time=time() - start)
        sys.exit(1)

    llt = time() - start  # llt = log loading time

    file_size = len(logfile.text)

    file_date = parser.parse(logfiledata['Last-Modified']).astimezone(tz)
    ## CONVERT AS TIMESTAMP:
    fdate = mktime(file_date.timetuple())

    """
    Avoid unnecessary updates, between updated time signature of distant file & local Redis
    value (called here 'ts').
    """
    if r.hexists('sodabar', 'ts'):
        redis_ts = r.hget('sodabar', 'ts').decode('utf-8')
        if float(float(redis_ts)) >= fdate:
            return False
        else:
            r.hset('sodabar', 'ts', fdate)
    else:
        r.hset('sodabar', 'ts', fdate)

    file_data = dict(updated_at=fdate,
                     file_size=file_size)

    if logfile.status_code == requests.codes.ok:
        logLines = logfile.text.split('\n')
        logs_obj = logsSerializer([log for log in logLines[:len(logLines) - 1]])
        logs_obj_size = len(logs_obj)

        # output all the objects if depth < 0, ELSE the last objects in the stack.
        output = logs_obj if depth < 0 else logs_obj[-depth:]

        if key is not None:
            # k = key.title()
            k = key
            out = []
            for r in output:
                out.append("{}: {}".format(k, r[k]))

            return out

        return dict(data=output,
                    access_log=llt,
                    file=file_data,
                    log_size=logs_obj_size)


def toRedis(content):
    """
    if content is None:
        return None
    """
    if content is False:
        return fromRedis()

    output = {}
    start = time()
    tsnow = 'sodabar'

    try:
        r.hset(tsnow, 'data', msgpack.packb(content['data']))
        output['access_log'] = content['access_log']
        output['file'] = content['file']
        output['log_size'] = content['log_size']
        output['cached'] = False
        output['error'] = False
    except KeyError as e:
        error_msg = "{}, KeyError exception >> {}".format(Message(), repr(e))
        output['error'] = True
        output['msg'] = error_msg

    output['elapsed'] = time() - start

    return (dict(cached=False,
                 data=fromRedis()[0]),
            output)


def fromRedis():
    start = time()
    try:
        log_date = float(r.hget('sodabar', 'ts').decode('utf-8'))
        human = datetime.datetime.fromtimestamp(log_date, tz=tz)
        last_visit_dict = dict(timestamp=log_date,
                               human_with_tz=human.strftime("%Y-%m-%d %H:%M:%S"))

        try:
            raw_data = r.hget('sodabar', 'data')
            pure_data = msgpack.unpackb(raw_data, encoding='utf-8')
            return (pure_data, dict(elapsed_from_redis=round(time() - start, 4),
                                    size_in_bytes=len(raw_data),
                                    visits=len(pure_data),
                                    log_size=len(pure_data),
                                    last_redis_update=last_visit_dict,
                                    error=False,
                                    cached=True))
        except Exception as e:
            error_msg = "{}, generic Exception >> {}".format(Message(), repr(e))
            #error_msg = "Error on persistance.py, fromRedis function, line 137, generic Exception ~> {}".format(repr(e))
            return (dict(error=True,
                        msg=error_msg
                        ),
                    dict(error=True,
                        msg=error_msg
                        )
                    )

    except AttributeError as e:
        return {'error': True,
                'msg': "{}, AttributeError exception >> {}".format(Message(), repr(e))}
                #'msg': "Error on persistence.py, fromRedis function, line 147, AttributeError exception ~> {}".format(str(e))}


def logsSerializer(log_list):
    """
    excluded: keys and values to remove (def in array)
    :param log_list: <list>
    :return: <list: dict>
    """
    excluded = ['latitude', 'longitude']
    parts = []
    for el in log_list:
        split_seq = dict(p.split(": ") for p in el.split(", "))
        # All keys in lowercase, please!
        ext_dict = {k.lower(): v for k, v in split_seq.items()}

        for k in excluded:
            if ext_dict[k]: del ext_dict[k]

        # convert key / value for Loading Time & date as <float>
        ext_dict['latency'] = float(ext_dict.pop('index_loading_performance').split('s')[0])
        ext_dict["ip"] = ext_dict.pop('client ip')
        # ext_dict["device"] = ext_dict.pop('Device')
        ext_dict["city"] = ext_dict.pop("city") if ext_dict["city"] != '' else 'Unknown'
        ext_dict["country"] = ext_dict.pop("country") if ext_dict["country"] != '' else 'Unknown'
        ext_dict["date"] = float(ext_dict.pop("when"))
        parts.append(ext_dict)
    return parts


if __name__ == "__main__":
    z = toRedis(kompakt(-1))
    w = fromRedis()
