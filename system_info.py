from persistance import r
import os
from subprocess import call

redis_cmd = 'info'


def cmd(command):
    out = {}
    infos = r.execute_command(redis_cmd, command).decode()
    infos = infos.split('\n')
    infos.pop(0)  # remove first element
    infos.pop(-1)  # remove last element
    inf = [it.replace('\r', '') for it in infos]
    for i in inf:
        z = i.split(':')
        # print(z)
        out[z[0]] = to_number(z[1])

    return out


def custom_cmd(command, allowed_keys=[]):
    full_dict = cmd(command)
    output = {}
    for wanted in full_dict.items():
        if wanted[0] in allowed_keys:
            output[wanted[0]] = wanted[1]

    return output

def to_number(x):
    try:
        float(x)
        return float(x)
    except ValueError:
        if x.isdigit():
            return int(x)
        else:
            return x


def mem():
    keys = ['total_system_memory',
            'total_system_memory_human',
            'used_memory',
            'used_memory_human',
            'used_memory_peak',
            'used_memory_peak_human',
            'used_memory_rss',
            'used_memory_rss_human']

    return custom_cmd('memory',
                      allowed_keys=keys)


def stats():
    keys = ['keyspace_hits',
            'total_commands_processed',
            'total_connections_received',
            'total_net_input_bytes',
            'total_net_output_bytes']

    return custom_cmd('stats',
                      allowed_keys=keys)


def cpu():
    return cmd('cpu')


def other_stats():
    return cmd('commandstats')


def keyspace():
    return cmd('keyspace')


def server():
    return cmd('server')


def persistance():
    return cmd('persistance')


def clients():
    return cmd('clients')

"""
def tail(long=10):
    logfile = os.path.join(os.path.dirname(__file__), 'task_logs', 'tasks.log')
    # return call(["tail", "-F", logfile])
    return call(['tail', '-n', str(long), logfile])
"""